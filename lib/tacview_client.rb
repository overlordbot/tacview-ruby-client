# frozen_string_literal: true

require_relative './tacview_client/version'
require_relative './tacview_client/client'

# Top-level namespace for all classes of the Ruby Tacview Client
module TacviewClient
end
