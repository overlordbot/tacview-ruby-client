# frozen_string_literal: true

require 'bigdecimal'
require_relative './reader/parser'

module TacviewClient
  # Reads events from an input source, parses them using the {Parser}
  # and calls the appropriate event processor method
  class Reader
    # The format that matches the beginning of an ACMI update line
    OBJECT_UPDATE_MARKER = '^\h\h+,'

    # The format that matches the beginning of an ACMI delete line
    OBJECT_DELETION_MARKER = '-'

    # The format that matches the beginning of an ACMI time update line
    TIME_UPDATE_MARKER = '#'

    # The format that matches the beginning of an ACMI Property line
    GLOBAL_PROPERTY_MARKER = '0,'

    # @param input_source [IO, #gets] An {IO} object (or object that implements
    #   the {IO#gets} method. Typically this is a Socket or File.
    # @param processor [BaseProcessor] The object that processes the events
    #   emitted by the {Reader}. Must implement the methods defined by the
    #   {BaseProcessor} and can optionally inherit from it.
    def initialize(input_source:, processor:)
      raise ArgumentError, 'input_source cannot be nil' if input_source.nil?
      raise ArgumentError, 'processor cannot be nil' if processor.nil?

      @input_source = input_source
      @processor = processor
    end

    def start_reading
      while (line = @input_source.gets)
        route_line(line.chomp)
      end
      true
    rescue SignalException
      exit
    ensure
      @input_source.close
    end

    private

    def route_line(line)
      if line.match?(OBJECT_UPDATE_MARKER)
        object_update(line)
      elsif line[0] == TIME_UPDATE_MARKER
        time_update(line)
      elsif line[0] == OBJECT_DELETION_MARKER
        object_deletion(line)
      elsif line[0..1] == GLOBAL_PROPERTY_MARKER
        global_property(line)
      end
    end

    def object_update(line)
      result = Parser.new.parse_object_update(line)
      @processor.update_object(result) if result
    end

    def object_deletion(line)
      @processor.delete_object line[1..-1]
    end

    def time_update(line)
      @processor.update_time BigDecimal(line[1..-1])
    end

    def global_property(line)
      key, value = line[2..-1].split('=')

      if value.end_with?('\\')
        value = [value] + read_multiline
        value = value.inject([]) do |arr, array_line|
          arr << array_line.delete('\\').strip
        end.join("\n")
      end

      @processor.set_property(property: key, value: value.strip)
    end

    def read_multiline
      array_lines = []
      while (line = @input_source.readline)
        array_lines << line
        break unless line.end_with?('\\')
      end

      array_lines
    end
  end
end
