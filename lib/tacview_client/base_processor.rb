# frozen_string_literal: true

module TacviewClient
  # An Base Procesor that defines all the methods called by an instance of
  # a TacviewClient::Reader. Use this as a guide on what you need to
  # implement. You can also optionally inherit from this class to make
  # sure you have every method defined in your sub-class
  class BaseProcessor
    # Process an update event for an object
    #
    # On the first appearance of the object there are usually more fields
    # including pilot names, object type etc.
    #
    # For existing objects these events are almost always lat/lon/alt updates
    # only
    #
    # @param event [Hash] A parsed ACMI line. This hash will always include
    #   the fields listed below but may also include others depending on the
    #   source line.
    # @option event [String] :object_id The hexadecimal format object ID.
    # @option event [BigDecimal] :latitude The object latitude in WGS 84 format.
    # @option event [BigDecimal] :longitude The object latitude in WGS 84
    #   format.
    # @option event [BigDecimal] :altitude The object altitude above sea level
    #   in meters to 1 decimal place.
    def update_object(event)
      raise NotImplementedError, 'To be implemented by subclass'
    end

    # Process a delete event for an object
    #
    # @param object_id [String] A hexadecimal number representing the object
    #   ID
    def delete_object(object_id)
      raise NotImplementedError, 'To be implemented by subclass'
    end

    # Process a time update event
    #
    # @param time [BigDecimal] A time update in seconds from the
    #   ReferenceTime to 2 decimal places
    def update_time(time)
      raise NotImplementedError, 'To be implemented by subclass'
    end

    # Set a property
    #
    # @param property [String] The name of the property to be set
    # @param value [String] The value of the property to be set
    def set_property(property:, value:)
      raise NotImplementedError, 'To be implemented by subclass'
    end
  end
end
