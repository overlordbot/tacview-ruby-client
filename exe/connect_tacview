#!/usr/bin/env ruby
# frozen_string_literal: true

require 'optparse'
require 'bundler/setup'
require 'tacview_client'
require 'tacview_client/base_processor'

options = {
  password: nil,
  port: 42_674,
  client_name: 'ruby_tacview_client'
}

OptionParser.new do |opts|
  opts.banner = 'Usage: ruby connect_tacview [options]'

  opts.on('-h', '--host=host', 'Tacview server hostname / IP') do |v|
    options[:host] = v
  end

  opts.on('-p', '--port=port', 'Tacview server port (Default: 42674)') do |v|
    options[:port] = v
  end

  opts.on('-a', '--password=password', 'Tacview server password') do |v|
    options[:password] = v
  end

  opts.on('-c', '--client-name=client', 'Client name (Default:' \
          'ruby_tacview_client') do |v|
    options[:client_name] = v
  end
end.parse!

if !options[:host] || !options[:host].is_a?(String) || options[:host].empty?
  puts 'Hostname required'
  exit(1)
end

if options[:client_name].empty?
  puts 'client-name cannot be empty'
  exit(1)
end

# Add console logging for this command-line app
module TacviewClientLogger
  def initialize(host:,
                 port: 42_674,
                 password: nil,
                 processor:,
                 client_name: 'ruby_tacview_client')

    print "Connecting to #{host}:#{port} as #{client_name}"
    puts password ? ' with a password supplied' : ''

    super(host: host, port: port, password: password, processor: processor,
          client_name: client_name)
  end

  private

  def read_handshake
    puts 'Reading Incoming Handshake'
    super
  end

  def read_handshake_header(header)
    print header.to_s.tr('_', ' ').capitalize + ': '
    result = super
    puts result
    result
  end

  def send_handshake
    puts 'Sending Handshake'
    super
  end

  def start_reader
    puts 'Starting event stream reader'
    puts '-' * 80
    super
  end
end
# A very simple processor that outputs all events received from a
# TacviewClient::Reader instance to the console. Useful for simple
# testing apps
class ConsoleOutputter < TacviewClient::BaseProcessor
  def update_object(object)
    puts "Object Update  : #{object}"
  end

  def delete_object(object_id)
    puts "Object Deletion: #{object_id}"
  end

  def update_time(time)
    puts "Time Update    : #{time}"
  end

  def set_property(property:, value:)
    puts "Property Set   : #{property}, #{value}"
  end
end

TacviewClient::Client.prepend TacviewClientLogger

client = TacviewClient::Client.new host: options[:host],
                                   port: options[:port],
                                   password: options[:password],
                                   client_name: options[:client_name],
                                   processor: ConsoleOutputter.new
client.connect
