# frozen_string_literal: true

require 'stringio'
require 'spec_helper'
require_relative '../../lib/tacview_client/reader'

describe TacviewClient::Reader do
  context 'during initialization' do
    let(:dummy_source) { Object.new }
    let(:dummy_processor) { Object.new }

    it 'requires an input source' do
      expect do
        described_class.new input_source: nil, processor: dummy_processor
      end.to raise_error ArgumentError, 'input_source cannot be nil'
    end

    it 'requires an processor' do
      expect do
        described_class.new input_source: dummy_source, processor: nil
      end.to raise_error ArgumentError, 'processor cannot be nil'
    end
  end

  context 'when reading an input source' do
    let(:simple_io) { StringIO.new("a\n", 'r') }
    let(:dummy_processor) { Object.new }

    it 'exits when the end of the input is reached' do
      reader = described_class.new input_source: simple_io,
                                   processor: :dummy_processor

      expect(reader.start_reading).to be_truthy
    end

    it 'exits when sent a termination signal'

    context 'when event processing' do
      let(:processor) { double('processor') }
      let(:object_id) { 'ad02' }

      context 'object update' do
        it 'ignores updates without position information' do
          no_position_io = StringIO.new "#{object_id},IAS=180"
          reader = described_class.new input_source: no_position_io,
                                       processor: processor

          expect(processor).not_to receive(:update_object)

          reader.start_reading
        end

        context 'position processing' do
          context 'processes lat/lon/alt' do
            context 'when all three values are present' do
              it 'returns the present values' do
                lat_lon_alt_io = StringIO.new(
                  "#{object_id},T=123.45|678.09|234.2"
                )
                reader = described_class.new input_source: lat_lon_alt_io,
                                             processor: processor

                expect(processor).to receive(:update_object).with(
                  object_id: object_id,
                  longitude: BigDecimal('123.45'),
                  latitude: BigDecimal('678.09'),
                  altitude: BigDecimal('234.2')
                )

                reader.start_reading
              end
            end

            context 'when latitude is empty' do
              it 'result does not include latitude' do
                lat_lon_alt_io = StringIO.new(
                  "#{object_id},T=|678.09|234.2"
                )
                reader = described_class.new input_source: lat_lon_alt_io,
                                             processor: processor

                expect(processor).to receive(:update_object).with(
                  object_id: object_id,
                  latitude: BigDecimal('678.09'),
                  altitude: BigDecimal('234.2')
                )

                reader.start_reading
              end
            end

            context 'when longitude is empty' do
              it 'result does not include longitude' do
                lat_lon_alt_io = StringIO.new(
                  "#{object_id},T=123.45||234.2"
                )
                reader = described_class.new input_source: lat_lon_alt_io,
                                             processor: processor

                expect(processor).to receive(:update_object).with(
                  object_id: object_id,
                  longitude: BigDecimal('123.45'),
                  altitude: BigDecimal('234.2')
                )

                reader.start_reading
              end
            end

            context 'when altitude is empty' do
              it 'result does not include altitude' do
                lat_lon_alt_io = StringIO.new(
                  "#{object_id},T=123.45|678.09|"
                )
                reader = described_class.new input_source: lat_lon_alt_io,
                                             processor: processor

                expect(processor).to receive(:update_object).with(
                  object_id: object_id,
                  longitude: BigDecimal('123.45'),
                  latitude: BigDecimal('678.09')
                )

                reader.start_reading
              end
            end
          end

          [
            'ad02,T=123.45|678.09|234.2||441.2',
            'ad02,T=123.45|678.09|234.2|321.1|441.2',
            'ad02,T=123.45|678.09|234.2|321.1|441.2|',
            'ad02,T=123.45|678.09|234.2|321.1|441.2|213.1',
            'ad02,T=123.45|678.09|234.2|321.1|441.2|213.1|'
          ].each do |message|
            it 'processes only lat/lon/alt of messages with ' \
               "#{message.scan(/\d+\.?\d*/).size} values and " \
               "#{message.count('|')} separators" do
              position_io = StringIO.new message
              reader = described_class.new input_source: position_io,
                                           processor: processor

              expect(processor).to receive(:update_object).with(
                object_id: object_id,
                longitude: BigDecimal('123.45'),
                latitude: BigDecimal('678.09'),
                altitude: BigDecimal('234.2')
              )

              reader.start_reading
            end
          end

          it 'handles negative numbers' do
            lat_lon_alt_io = StringIO.new(
              "#{object_id},T=-123.45|-678.09|-234.2"
            )
            reader = described_class.new input_source: lat_lon_alt_io,
                                         processor: processor

            expect(processor).to receive(:update_object).with(
              object_id: object_id,
              longitude: BigDecimal('-123.45'),
              latitude: BigDecimal('-678.09'),
              altitude: BigDecimal('-234.2')
            )

            reader.start_reading
          end

          it 'processes the heading when present' do
            lat_lon_alt_io = StringIO.new(
              "#{object_id},T=123.45|678.09|234.2|||||359"
            )
            reader = described_class.new input_source: lat_lon_alt_io,
                                         processor: processor

            expect(processor).to receive(:update_object).with(
              object_id: object_id,
              longitude: BigDecimal('123.45'),
              latitude: BigDecimal('678.09'),
              altitude: BigDecimal('234.2'),
              heading: BigDecimal('359')
            )

            reader.start_reading
          end
        end

        context 'freeform field processing' do
          it 'processes each field' do
            freeform_io = StringIO.new(
              "#{object_id},T=123.45|678.09|234.2" \
              'Type=Air+FixedWing,Name=FA-18C_hornet,Pilot==RVE=Example,' \
              'Group=**5) [CAP] F/A-18C (Sochi) #006,Color=Blue,' \
              'Coalition=Enemies,Country=au'
            )
            reader = described_class.new input_source: freeform_io,
                                         processor: processor
            expect(processor).to receive(:update_object).with(
              object_id: object_id,
              longitude: BigDecimal('123.45'),
              latitude: BigDecimal('678.09'),
              altitude: BigDecimal('234.2'),
              coalition: 'Enemies',
              color: 'Blue',
              country: 'au',
              group: '**5) [CAP] F/A-18C (Sochi) #006',
              name: 'FA-18C_hornet',
              pilot: '=RVE=Example'
            )

            reader.start_reading
          end
        end
      end

      context 'object deletion' do
        let(:object_deletion_io) { StringIO.new("-#{object_id}", 'r') }

        it 'calls processor #delete_object method with the object ID' do
          reader = described_class.new input_source: object_deletion_io,
                                       processor: processor
          expect(processor).to receive(:delete_object).with(object_id)

          reader.start_reading
        end
      end

      context 'time update' do
        let(:seconds) { '7.73' }
        let(:time_update_io) { StringIO.new("##{seconds}", 'r') }

        it 'calls the processor #update_time method with the time value' do
          reader = described_class.new input_source: time_update_io,
                                       processor: processor
          expect(processor).to receive(:update_time).with(BigDecimal(seconds))

          reader.start_reading
        end
      end

      context 'global property' do
        context 'single-line' do
          let(:key) { 'ReferenceLongitude' }
          let(:value) { '35' }
          let(:global_property_io) { StringIO.new("0,#{key}=#{value}", 'r') }

          it 'calls the processor #set_property method with the property' do
            reader = described_class.new input_source: global_property_io,
                                         processor: processor
            expect(processor).to receive(:set_property)
              .with(property: key, value: value)

            reader.start_reading
          end
        end

        context 'multi-line' do
          let(:key) { 'Comment' }
          let(:value) do
            [
              'line 1 \\',
              'line 2'
            ].join("\n")
          end
          let(:global_property_io) { StringIO.new("0,#{key}=#{value}", 'r') }

          it 'calls the processor #set_property method with the property ' do
            reader = described_class.new input_source: global_property_io,
                                         processor: processor
            expect(processor).to receive(:set_property)
              .with(property: key, value: "line 1\nline 2")

            reader.start_reading
          end
        end
      end
    end
  end
end
