# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/tacview_client/client'

describe TacviewClient::Client do
  context 'connecting to a server' do
    let(:host) { 'dummy_host' }
    let(:processor) { Object.new }
    let(:valid_server) do
      StringIO.new [
        'XtraLib.Stream.0',
        'Tacview.RealTimeTelemetry.0',
        'Dummy Host'
      ].join("\n") + "\0"
    end

    context 'validates the incoming handshake' do
      it 'and does not exit with code 1 if everything is correct' do
        allow(TCPSocket).to receive(:open) { valid_server }

        described_class.new(host: host, processor: processor).connect
      rescue SystemExit => e
        expect(e.status).to eq(0)
      end

      it 'and exits with code 1 if stream protocol is not recognised' do
        bad_stream_protocol = StringIO.new [
          'Dummy.Stream.Protocol.0',
          'Tacview.RealTimeTelemetry.0',
          'Dummy Host'
        ].join("\n") + "\0"

        allow(TCPSocket).to receive(:open) { bad_stream_protocol }

        described_class.new(host: host, processor: processor).connect
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end

      it 'and exits with code 1 if tacview protocol is not recognised' do
        bad_tacview_protocol = StringIO.new [
          'XtraLib.Stream.0',
          'Tacview.RealTimeTelemetry.-1',
          'Dummy Host'
        ].join("\n") + "\0"

        allow(TCPSocket).to receive(:open) { bad_tacview_protocol }

        described_class.new(host: host, processor: processor).connect
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end
    end

    context 'sending the handshake' do
      context 'when password and client_name are not specified' do
        it 'sends the outgoing default handshake' do
          outgoing_handshake = [
            'XtraLib.Stream.0',
            'Tacview.RealTimeTelemetry.0',
            'ruby_tacview_client',
            0
          ].join("\n") + "\0"

          allow(TCPSocket).to receive(:open) { valid_server }

          expect(valid_server).to receive(:print).with(outgoing_handshake)

          described_class.new(host: host, processor: processor).connect
        end
      end

      context 'when password is specified' do
        it 'sends the outgoing handshake with the hashed password' do
          # rubocop:disable Style/NumericLiterals
          password = { plain: 'DummyPassword', hashed: 2720314366582886589 }
          # rubocop:enable Style/NumericLiterals

          outgoing_handshake = [
            'XtraLib.Stream.0',
            'Tacview.RealTimeTelemetry.0',
            'ruby_tacview_client',
            password[:hashed]
          ].join("\n") + "\0"

          allow(TCPSocket).to receive(:open) { valid_server }

          expect(valid_server).to receive(:print).with(outgoing_handshake)

          described_class.new(host: host,
                              processor: processor,
                              password: password[:plain]).connect
        end
      end

      context 'when client_name is specified' do
        it 'sends the outgoing handshake with the hashed password' do
          specified_name = 'RSpec client'

          outgoing_handshake = [
            'XtraLib.Stream.0',
            'Tacview.RealTimeTelemetry.0',
            specified_name,
            0
          ].join("\n") + "\0"

          allow(TCPSocket).to receive(:open) { valid_server }

          expect(valid_server).to receive(:print).with(outgoing_handshake)

          described_class.new(host: host,
                              processor: processor,
                              client_name: specified_name).connect
        end
      end
    end

    it 'starts the reader' do
      allow(TCPSocket).to receive(:open) { valid_server }

      reader = double('Reader').as_null_object
      allow(TacviewClient::Reader)
        .to receive(:new) { reader }

      expect(TacviewClient::Reader) .to receive(:new)
        .with(input_source: valid_server,
              processor: processor)
      expect(reader).to receive(:start_reading)

      described_class.new(host: host, processor: processor).connect
    end
  end
end
