# Tacview Client

A Ruby client that speaks the [Tacview](http://www.tacview.net) protocol and
can connect to a Tacview compatible server.

This Gem is tested against the latest non-preview releases of Ruby and JRuby.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tacview_client'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tacview_client

## Usage

### API

To connect to a Tacview server with this library you can use the following code

```ruby

require 'tacview_client'

# See docs for all parameters
client = TacviewClient::Client.new host: 127.0.0.1,
                                   processor: ProcessorInstance
```

This gem uses Inversion Of Control principles and expects you to provide an
object to receive events for processing. This can be a `Module` or a `Class` 
and it must implement the methods descibed in the `BaseProcessor` class. See
the documentation of this class for more information.

For an example of this see the `ConsoleOutputter` module in the
`exe/connect_tacview` file

#### Logging

This gem provides no logging out of the box. If you want to add logging then
the recommended approach is to create your own logger class and use
`Module.prepend` to prepend it to the `Client` class (For logging connection
information) or the `Reader` class's `#route_line` method for logging of the
raw ACMI lines

For an example of this see the `TacviewClientLogger` module in the
`exe/connect_tacview` file

### Command-line

This gem provides a command-line application. Use `connect_tacview --help` for
invocation information. Thie command-line will connect to a Tacview server and
print some connection debug information followed by the parsed event stream to
the terminal 

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then run
`rake` to run rubocop and the tests. You can also run `bin/console` for an
interactive prompt that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on [GitLab](https://gitlab.com/overlord-bot/tacview-ruby-client).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
